# OpenML dataset: Internet-Advertisements

https://www.openml.org/d/40978

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Nicholas Kushmerick  
**Source**: [UCI](http://archive.ics.uci.edu/ml/datasets/Internet+Advertisements) - 1998  
**Please cite**:   

### Description

__Changes to version 1:__ all categorical features transformed as such. 

This dataset represents a set of possible advertisements on Internet pages. 

### Sources

(a) Creator and donor:

Nicholas Kushmerick - nick@ucd.ie

### Dataset Information

The features encode the geometry of the image (if available) as well as phrases occurring in the URL, the image's URL and alt text, the anchor text, and words occurring near the anchor text. The task is to predict whether an image is an advertisement ("ad") or not ("nonad").

### Atributtes Information

There are : 3 continuous attributes. The others are binary.
This is the "STANDARD encoding" mentioned in the [Kushmerick, 99] (see below). 
One or more of the three continuous features are missing in 28% of the instances.
Missing values should be interpreted as "unknown".

### Relevant Papers  

N. Kushmerick (1999). "Learning to remove Internet advertisements", 3rd Int Conf Autonomous Agents.  
Available at: http://rexa.info/paper/2fdc1cee89b7f4f2c9227d6f5d9b05d22c5ab3e9

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40978) of an [OpenML dataset](https://www.openml.org/d/40978). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40978/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40978/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40978/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

